﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace Phoenix.Windows.Controls
{
    /// <summary>
    /// Interaction logic for ToggleSwitchButton.xaml
    /// </summary>
    public partial class ToggleSwitchButton
    {
        public static readonly DependencyProperty IsCheckedProperty = DependencyProperty.Register("IsChecked", typeof(bool?), typeof(ToggleSwitchButton), new FrameworkPropertyMetadata(
            (bool?)null,
            OnIsCheckedChanged
            )
        );

        public bool? IsChecked
        {
            get { return (bool?)GetValue(IsCheckedProperty); }
            set
            {
                SetValue(IsCheckedProperty, value);
                IsToggled = value ?? false;
            }
        }

        private static void OnIsCheckedChanged(DependencyObject src, DependencyPropertyChangedEventArgs e)
        {
            if (src is ToggleSwitchButton s) s.IsChecked = (bool)e.NewValue;
        }

        public ToggleSwitchButton()
        {
            InitializeComponent();
        }

        public bool IsToggled
        {
            set
            {
                if (tbStatus == null)
                {
                    return;
                }

                toggleBorder.Background = value ? Brushes.SpringGreen : Brushes.Gray;
                tbStatus.Text = (value ? "Enabled" : "Disabled");
                bOff.Visibility = (value ? Visibility.Hidden : Visibility.Visible);
                bOn.Visibility = (value ? Visibility.Visible : Visibility.Hidden);
            }
        }

        private void UIElement_OnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            IsChecked = !IsChecked;
        }
    }
}