﻿using System.Diagnostics;

namespace Phoenix.Windows.Common
{
    public class ProcessItem
    {
        public int Pid { get; set; }
        public string ProcessName { get; set; }
        public string ApplicationName { get; set; }
        public ProcessPriorityClass Priority { get; set; }
    }
}
