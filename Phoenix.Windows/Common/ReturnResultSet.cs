﻿using DeepArmor.Client.CommonLibrary.Enums;
using NLog;
using System;

namespace Phoenix.Windows.Common
{
    public class ReturnResultSet<T>
    {
        public T ObjectValue { get; set; }

        public Exception ResultException { get; set; }

        public string AdditionalErrorText { get; set; }

        public bool HasError => ResultException != null;

        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        public ReturnResultSet(Exception exception, string additionalErrorText = null, LoggingLevel logLevel = LoggingLevel.ERROR)
        {
            ResultException = exception;
            AdditionalErrorText = additionalErrorText;

            if (exception == null)
            {
                return;
            }

            var message = $"Return result set exception ({AdditionalErrorText}): {exception}";

            switch (logLevel)
            {
                case LoggingLevel.DEBUG:
                    log.Debug(message);
                    break;
                case LoggingLevel.INFO:
                    log.Info(message);
                    break;
                case LoggingLevel.WARN:
                    log.Warn(message);
                    break;
                case LoggingLevel.ERROR:
                    log.Error(message);
                    break;
                default:
                    log.Error(message);
                    break;
            }
        }

        public ReturnResultSet(T objectValue)
        {
            ObjectValue = objectValue;
        }
    }
}
