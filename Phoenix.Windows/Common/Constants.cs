﻿namespace Phoenix.Windows.Common
{
    public static class Constants
    {
        public const string BROWSE_FILTERS = "CSV Files (*.csv)|*.csv";

        public readonly static string MSG_PRIORITY_REPORT = "Status: Completed \r\nNumber of Process(es): {0} \r\nPriority Updated: {1}";

        public const string APP_NAME = "Phoenix";
        public const int PROCESS_NAME_LENGTH = 4000;
    }
}
