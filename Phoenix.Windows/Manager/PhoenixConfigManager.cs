﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog.Fluent;
using Phoenix.Windows.Common;
using System;
using System.IO;
using System.Text;

namespace Phoenix.Windows.Manager
{
    public class PhoenixConfigManager
    {
        private static readonly string FileName = "Phoenix.config";
        private static readonly string ConfigPath = @".\" + FileName;
        public PhoenixConfigManager()
        {
        }

        /// <summary>
        /// Reads and returns configuration from encrypted json file if it exists else reads from plain json file. 
        /// </summary>
        public ReturnResultSet<PhoenixConfig> GetConfig()
        {
            ReturnResultSet<PhoenixConfig> result = null;

            var doesExist = File.Exists(ConfigPath);
            if (doesExist)
            {
                result = ReadFile<PhoenixConfig>(ConfigPath);
                return new ReturnResultSet<PhoenixConfig>(result.ObjectValue);
            }

            var defaultConfig = new PhoenixConfig();
            WriteConfig(defaultConfig);
            return new ReturnResultSet<PhoenixConfig>(defaultConfig);
        }
        public ReturnResultSet<T> ReadFile<T>(string fileName)
        {
            try
            {
                if (!File.Exists(fileName))
                {
                    Log.Error($"File was not found {fileName}");
                    throw new Exception($"File was not found {fileName}");
                }

                if (typeof(T) == typeof(byte[]))
                {
                    var bytes = File.ReadAllBytes(fileName);

                    return new ReturnResultSet<T>((T)Convert.ChangeType(bytes, typeof(T)));
                }

                var str = File.ReadAllText(fileName, Encoding.Default);

                if (typeof(T) == typeof(string))
                {
                    return new ReturnResultSet<T>((T)Convert.ChangeType(str, typeof(T)));
                }

                return new ReturnResultSet<T>(JObject.Parse(str).ToObject<T>());
            }
            catch (Exception ex)
            {
                return new ReturnResultSet<T>(ex, fileName);
            }
        }
        public void WriteConfig(PhoenixConfig config)
        {
            File.WriteAllBytes(FileName, Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(config)));
        }
    }

    public class PhoenixConfig
    { 
        public PhoenixConfig()
        {
            this.HistoryDisplayCount = 2;
        }
        public int HistoryDisplayCount { get; set; }
    }
}
