﻿using System.Windows;
using System.Windows.Input;

using Phoenix.Windows.Common;

namespace Phoenix.Windows
{
    public partial class MainWindow : Window
    {
        MessagePopup messagePopup;
        private ICommand _taskbarIconDoubleClickCommand;

       // private MainViewModel viewModel => (MainViewModel)DataContext;

        public MainWindow()
        {
            InitializeComponent();

            //if (DataContext == null || !(DataContext is MainViewModel))
            //{
            //    DataContext = new MainViewModel();
            //}

            tbiMain.LeftClickCommand = TaskbarIconDoubleClickCommand;
            tbiMain.DoubleClickCommand = TaskbarIconDoubleClickCommand;
        }

        /// <summary>
        /// Initialize RelayCommand to invoke application from system tray on double click.
        /// </summary>
        private ICommand TaskbarIconDoubleClickCommand => _taskbarIconDoubleClickCommand ?? (_taskbarIconDoubleClickCommand = new RelayCommand(TaskbarIconDoubleClick));

        /// <summary>
        /// Make application draggable from left mouse down.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        /// <summary>
        /// Bring application in front, show in taskbar and set window state to normal on icon double click from system tray.
        /// </summary>
        private void TaskbarIconDoubleClick()
        {
            Activate();
            ShowInTaskbar = true;
            WindowState = WindowState.Normal;
        }

        /// <summary>
        /// Set window state to normal when 'Open' is clicked from system tray.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Open_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Normal;
        }

        /// <summary>
        /// Shutdown application when 'Exit' is clicked from system tray.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Browse and select CSV file
        /// Calls ProcessContent method to get list of processes name in file
        /// Pass the list to UpdateProcessPriority method to update process priority
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnBrowse_Click(object sender, RoutedEventArgs e)
        {
            //using (var fileDialog = new System.Windows.Forms.OpenFileDialog())
            //{
            //    fileDialog.Filter = Constants.BROWSE_FILTERS;
            //    var dialogResult = fileDialog.ShowDialog();

            //    if (dialogResult.ToString() == "OK")
            //    {
            //        var filePath = fileDialog.FileName;

            //        var ProcessesList = viewModel.ProcessContent(filePath);

            //        var prioritiesUpdated = viewModel.UpdateProcessPriority(ProcessesList);

            //        var message = string.Format(Constants.MSG_PRIORITY_REPORT, ProcessesList.Count, prioritiesUpdated);

            //        messagePopup = new MessagePopup(message);
            //        messagePopup.Show();
            //    }
            //}
        }

        /// <summary>
        /// Minimize application to taskbar when clicked on '_' image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnMinimize_OnClick(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        /// <summary>
        /// Hide application from front when clicked on 'x' image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClose_OnClick(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
            ShowInTaskbar = false;
        }
    }
}
