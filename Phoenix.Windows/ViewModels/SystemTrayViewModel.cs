﻿using NLog;
using System;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Linq;
using Phoenix.Windows.Objects;
using Phoenix.Windows.Enum;
using Phoenix.Windows.ExtensionMethods;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Phoenix.Windows.Manager;using System.Management;
using Phoenix.Windows.Common;
using System.Dynamic;
using System.Threading.Tasks;

namespace Phoenix.Windows.ViewModels
{
    class SystemTrayViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Constants & Declarations

        private static Logger log = LogManager.GetCurrentClassLogger();
        public static PhoenixConfig PhoenixConfig = new PhoenixConfig();

        private List<ProcessPriorityUpdationHistory> PriorityUpdationHistory = new List<ProcessPriorityUpdationHistory>();

        private ObservableCollection<ProcessData> _ListOfActiveProcesses;
        public ObservableCollection<ProcessData> ListOfActiveProcesses
        {
            get
            {
                return _ListOfActiveProcesses;
            }
            set
            {
                _ListOfActiveProcesses = value;
                OnPropertyChanged("ListOfActiveProcesses");
            }
        }

        private ObservableCollection<ProcessData> _ListOfDisplayProcesses;
        public ObservableCollection<ProcessData> ListOfDisplayProcesses
        {
            get
            {
                return _ListOfDisplayProcesses;
            }
            set
            {
                _ListOfDisplayProcesses = value;
                OnPropertyChanged("ListOfDisplayProcesses");
            }
        }

        #endregion Const & Declarations

        public SystemTrayViewModel()
        {
            PhoenixConfig = new PhoenixConfigManager().GetConfig().ObjectValue;
            ListOfActiveProcesses = new ObservableCollection<ProcessData>();
        }

        #region Priority Setting and Updation 

        /// <summary>
        /// Iterate processes list and pass the names to update their priority. 
        /// and skip the processes names given through CSV
        /// </summary>
        /// <param name="ProcessesList"></param>
        /// <param name="CSVProcessesList"></param>
        /// <returns> number of updated priorities update</returns>
        internal int UpdateProcessPriority(ObservableCollection<ProcessData> ProcessesList, List<string> CSVProcessesList)
        {
            int prioritiesUpdated = 0;

            foreach (var eachProcess in ProcessesList)
            {
                try
                {
                    // skip any processes set by the CSV List or is Reverted
                    if (CSVProcessesList.Any(x => x == eachProcess.ProcessName) || eachProcess.IsReverted || eachProcess.IsTop3CpuUser)
                    {
                        if (eachProcess.IsTop3CpuUser)
                        {
                            if (eachProcess.IsFromCSV != true && eachProcess.CurrentCpuUsage != 0)
                            {
                                eachProcess.IsTop3CpuUser = true;
                                SetProcessPriority(GetProcessByName(eachProcess.ProcessName), ProcessPriorityClass.BelowNormal, true);
                                eachProcess.UpdatedPriority = ProcessPriorityClass.BelowNormal;
                                eachProcess.UpdateActions("Below Normal Execution");
                            }
                            
                        }
                        if (!eachProcess.IsTop3CpuUser && eachProcess.ProcessName == GetForegroundWindowProcessName())
                        {
                            continue;
                        }
                        
                    }
                    /// for handling active foreground window currently focused on
                    if (eachProcess.ProcessName == GetForegroundWindowProcessName())
                    {
                        if (eachProcess.UpdatedPriority != ProcessPriorityClass.High)
                            if (SetProcessPriority(GetProcessByName(eachProcess.ProcessName), ProcessPriorityClass.High))
                            {
                                eachProcess.UpdateActions("Optimized Execution");
                                eachProcess.UpdatedPriority = ProcessPriorityClass.High;
                                prioritiesUpdated++;
                            }
                    }
                    /// for handling all windows other than the active forground window
                    else if(!eachProcess.IsTop3CpuUser)
                    {
                        switch (eachProcess.CurrentWindowState)
                        {
                            /// for handling all minimized windows
                            case WindowCurrentState.Minimized:

                                if (eachProcess.UpdatedPriority != ProcessPriorityClass.BelowNormal)
                                {
                                    if (SetProcessPriority(GetProcessByName(eachProcess.ProcessName), ProcessPriorityClass.BelowNormal))
                                    {
                                        eachProcess.UpdateActions("Below Normal Execution");
                                        eachProcess.UpdatedPriority = ProcessPriorityClass.BelowNormal;
                                        prioritiesUpdated++;
                                    }
                                }
                                break;
                            /// for handling all other visibile windows other than the active forground window
                            default:
                                if (eachProcess.UpdatedPriority != ProcessPriorityClass.Normal)
                                {
                                    if (SetProcessPriority(GetProcessByName(eachProcess.ProcessName), ProcessPriorityClass.Normal))
                                    {
                                        eachProcess.UpdateActions("Normal Execution");
                                        eachProcess.UpdatedPriority = ProcessPriorityClass.Normal;
                                        prioritiesUpdated++;
                                    }
                                }
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error($"Unable to set Priority for Process: {eachProcess}, {ex.StackTrace.ToString()}");
                }
                SetChildProcessesPriority(eachProcess);
            }

            return prioritiesUpdated;
        }

        /// <summary>
        /// Iterate processes list given via CSV to update their priority.
        /// </summary>
        /// <param name="CSVProcessesList"></param>
        /// <returns></returns>
        internal int UpdateProcessPriorityFromCSV(List<string> CSVProcessesList)
        {
            int prioritiesUpdated = 0;
            foreach (var eachProcess in CSVProcessesList)
            {
                try
                {
                    if (SetProcessPriority(GetProcessByName(eachProcess), ProcessPriorityClass.High))
                    {
                        prioritiesUpdated++;
                        var activeProcess = ListOfActiveProcesses.SingleOrDefault(x => x.ProcessName == eachProcess);
                        if (activeProcess != null)
                        {
                            if (activeProcess.UpdatedPriority != ProcessPriorityClass.High)
                            {
                                activeProcess.UpdateActions("Optimized Execution");
                            }
                            activeProcess.IsFromCSV = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error($"Unable to set Priority for Process: {eachProcess}, {ex.StackTrace.ToString()}");
                }
            }
            return prioritiesUpdated;
        }

        /// <summary>
        /// Update processes priority
        /// </summary>
        /// <param name="Processes"></param>
        /// <param name="ProcessPriorityClass"></param>
        private bool SetProcessPriority(Process[] Processes, ProcessPriorityClass NewProcessPriority, bool forceChange = true)
        {
            var PriorityUpdated = false;
            if (Processes != null)
            {
                foreach (var process in Processes)
                {
                    
                    try
                    {
                        if (!process.HasExited)
                        {
                            if (process.PriorityClass != NewProcessPriority || forceChange)
                            {
                                process.PriorityClass = NewProcessPriority;
                                PriorityUpdated = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error($"Unable to set Priority for Process: {process},id: {process.Id}, {ex.StackTrace.ToString()}");
                        throw ex;
                    }
                }
            }
            return PriorityUpdated;
        }

        public List<Process> SetChildProcessesPriority(ProcessData process)
        {
            List<Process> children = new List<Process>();
            ManagementObjectSearcher mos = new ManagementObjectSearcher(String.Format("Select * From Win32_Process Where ParentProcessID={0}", process.Pid));

            foreach (ManagementObject mo in mos.Get())
            {
                children.Add(Process.GetProcessById(Convert.ToInt32(mo["ProcessID"])));
            }

            SetProcessPriority(children.ToArray(), (ProcessPriorityClass)process.UpdatedPriority);
            return children;
        }

        internal void RevertProcessToOrignalState(int ProcessId, bool RevertAll = false)
        {
            if (RevertAll == false)
            {
                var ProcessToRevert = ListOfActiveProcesses.SingleOrDefault(x => x.Pid == ProcessId);
                if (!ProcessToRevert.IsReverted)
                {
                    ProcessToRevert.IsReverted = true;
                    SetProcessPriority(GetProcessByName(ProcessToRevert.ProcessName), ProcessToRevert.OrignalPriority);

                    ProcessToRevert.action = "Reverted to " + ProcessToRevert.OrignalPriority.ToString().ToPhoenixPriorityString();
                    ProcessToRevert.actionsList.Insert(0, "Reverted to " + ProcessToRevert.OrignalPriority.ToString().ToPhoenixPriorityString());
                    ProcessToRevert.actionsList = new ObservableCollection<string>();
                }
            }
            else
            {
                foreach (var proc in ListOfActiveProcesses)
                {
                    if (proc.UpdatedPriority != proc.OrignalPriority)
                    {
                        SetProcessPriority(GetProcessByName(proc.ProcessName), proc.OrignalPriority);
                    }
                }
            }
        }

        #endregion Priority Setting and Updation 

        #region Get Processes Functions


        /// <summary>
        /// Gets All Processes running by user
        /// </summary>
        /// <returns></returns>
        internal bool GetAllProcesses()
        {
            try
            {
                List<ProcessData> TopProcData = GetTopCpuUsingApps();

                foreach (var topProc in TopProcData)
                {
                    if (topProc.IsFromCSV != true && topProc.CurrentCpuUsage != 0)
                    {
                        ListOfActiveProcesses.SingleOrDefault(x => x.Pid == topProc.Pid).IsTop3CpuUser = true;

                    }
                }
                bool IsNewProcessAdded = false;
                // Only get applcations which are not service
                var processes = Process.GetProcesses().Where(p => p.MainWindowHandle.ToInt32() != 0).ToArray();

                ListOfActiveProcesses.ToList().ForEach(c => c.IsAlive = false);

                foreach (var pr in processes)
                {
                    //if Process is new and is not already in our Program
                    if (!ListOfActiveProcesses.Any(x => x.ProcessName == pr.ProcessName))
                    {
                        //skipping known apps which change prioirty themselves
                        if (pr.ProcessName.ToLower() == "syntpenh" || pr.ProcessName.ToLower() == "explorer" || pr.ProcessName.StartsWith(Constants.APP_NAME))
                        {
                            continue;
                        }
                        ProcessData proc = new ProcessData();

                        proc.IsAlive = true;
                        proc.Pid = pr.Id;
                        proc.DisplayName = pr.ProcessName.ToFirstLetterUpper();
                        proc.ProcessName = pr.ProcessName;
                        proc.CurrentWindowState = GetPlacement(pr.MainWindowHandle).showCmd;

                        var result = GetProcessName(pr.Id);

                        proc.ApplicationName = result.ApplicationName;
                        proc.ApplicationIcon = result.ApplicationIcon.ToBitmapImage();

                        try
                        {
                            proc.OrignalPriority = pr.PriorityClass;
                            proc.UpdatedPriority = pr.PriorityClass;

                            proc.UpdateActions(pr.PriorityClass.ToString().ToPhoenixPriorityString());

                        }
                        catch (Exception ex)
                        {
                            log.Error($"Unable to get Priority for Process: {proc.ProcessName} with pid: {proc.ProcessName}, {ex.StackTrace.ToString()}");
                            continue;
                        }

                        ListOfActiveProcesses.Add(proc);
                        IsNewProcessAdded = true;
                    }
                    //update the MainWindowState
                    else
                    {
                        if (ListOfActiveProcesses.Count > 0)
                        {
                            var process = ListOfActiveProcesses.SingleOrDefault(x => x.ProcessName == pr.ProcessName);
                            process.CurrentWindowState = GetPlacement(pr.MainWindowHandle).showCmd;
                            process.IsAlive = true;

                        }
                    }
                }
                #region Remove Process that are no longer Alive

                var itemsToRemove = ListOfActiveProcesses.Where(x => x.IsAlive == false).ToList();

                foreach (var itemToRemove in itemsToRemove)
                {
                    ListOfActiveProcesses.Remove(itemToRemove);
                }

                #endregion Remove Process that are no longer Alive
                return IsNewProcessAdded;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Get all instances of given process name running on the local computer.
        /// This will return an empty array if process isn't running.
        /// </summary>
        /// <param name="ProcessName"></param>
        /// <returns></returns>
        private Process[] GetProcessByName(string ProcessName) => Process.GetProcessesByName(ProcessName);



        private string GetForegroundWindowProcessName()
        {
            var hwnd = GetForegroundWindow();
            Int32 processId = 0;
            GetWindowThreadProcessId(hwnd, out processId);
            if (processId != 0)
            {
                return Process.GetProcessById(processId).ProcessName;
            }
            return string.Empty;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static (string ApplicationName, Bitmap ApplicationIcon) GetProcessName(int pid)
        {

            var processHandle = OpenProcess(0x0400 | 0x0010, false, pid);

            if (processHandle == IntPtr.Zero)
            {
                return (null, null);
            }

            const int lengthSb = 4000;

            var sb = new StringBuilder(lengthSb);

            string appName = null;
            Bitmap appIcon = null;
            if (GetModuleFileNameEx(processHandle, IntPtr.Zero, sb, lengthSb) > 0)
            {
                appName = Path.GetFileName(sb.ToString());
                try
                {
                    appIcon = Icon.ExtractAssociatedIcon(Path.GetFullPath(sb.ToString())).ToBitmap();
                }
                catch // If No icon related to file path found
                {

                }
            }

            CloseHandle(processHandle);

            return (appName, appIcon);
        }

        private static WINDOWPLACEMENT GetPlacement(IntPtr hwnd)
        {
            try
            {
                WINDOWPLACEMENT placement = new WINDOWPLACEMENT();
                placement.length = Marshal.SizeOf(placement);
                GetWindowPlacement(hwnd, ref placement);
                return placement;
            }
            catch
            {
                return new WINDOWPLACEMENT();
            }
        }
        #endregion Get Processes Functions

        #region Get Process Window State


        #region Extern functions

        #region for Get All Process

        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(uint processAccess, bool bInheritHandle, int processId);

        [DllImport("psapi.dll")]
        static extern uint GetModuleFileNameEx(IntPtr hProcess, IntPtr hModule, [Out] StringBuilder lpBaseName, [In] [MarshalAs(UnmanagedType.U4)] int nSize);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool CloseHandle(IntPtr hObject);
        #endregion for Get All Process

        #region for Get ForegroundWindow

        [DllImport("user32.dll")]
        static extern int GetForegroundWindow();

        [DllImport("user32")]
        private static extern UInt32 GetWindowThreadProcessId(Int32 hWnd, out Int32 lpdwProcessId);

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool GetWindowPlacement(
            IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);

        #endregion for Get ForegroundWindow

        #endregion Extern functions



        [Serializable]
        internal struct WINDOWPLACEMENT
        {
            public int length;
            public int flags;
            public WindowCurrentState showCmd;
            public Point ptMinPosition;
            public Point ptMaxPosition;
            public Rectangle rcNormalPosition;
        }



        #endregion Get Process Window State

        #region Process CSV File

        /// <summary>
        /// Extracts processes name from CSV file and populate list with the names
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        internal List<string> ProcessContent(string filePath)
        {
            try
            {
                var ProcessesList = new List<string>();
                var delimiters = new char[] { ',' };
                using (var reader = new StreamReader(filePath))
                {
                    while (!reader.EndOfStream)
                    {
                        var processes = reader.ReadLine().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

                        ProcessesList.AddRange(processes);
                    }
                }

                return ProcessesList;
            }
            catch
            {
                return new List<string>();
            }
        }

        #endregion Process CSV Files

        #region Processes taking Max Cpu Usage
        

        public List<ProcessData> GetTopCpuUsingApps()
        {
            List<ProcessData> TopProcData = new List<ProcessData>();

            ListOfActiveProcesses.Where(a => a.IsTop3CpuUser == true).ToList().ForEach(c => c.UpdatedPriority = c.OrignalPriority);

            ListOfActiveProcesses.ToList().ForEach(c => c.IsTop3CpuUser = false);

            List<ProcessData> top3thing = new List<ProcessData>();
            foreach (var process in ListOfActiveProcesses)
            {
                // Preparing variable for application instance name
                var name = string.Empty;

                foreach (var instance in new PerformanceCounterCategory("Process").GetInstanceNames())
                {
                    if (instance.StartsWith(process.ProcessName))
                    {
                        using (var processId = new PerformanceCounter("Process", "ID Process", instance, true))
                        {
                            if (process.Pid == (int)processId.RawValue)
                            {
                                name = instance;
                                break;
                            }
                        }
                    }
                }

                var cpu = new PerformanceCounter("Process", "% Processor Time", name, true);
                var ram = new PerformanceCounter("Process", "Private Bytes", name, true);

                // Getting first initial values
                cpu.NextValue();
                ram.NextValue();

                // Creating delay to get correct values of CPU usage during next query
                //System.Threading.Thread.Sleep(100);

                // If system has multiple cores, that should be taken into account
                process.CurrentCpuUsage = Math.Round(cpu.NextValue() / Environment.ProcessorCount, 2);
                TopProcData.Add(process);
            }

            return TopProcData = new List<ProcessData>(TopProcData.GroupBy(i => i.ProcessName).Select(i => new ProcessData
            {
                IsAlive = true,
                Pid = i.First().Pid,
                DisplayName = i.First().ProcessName.ToFirstLetterUpper(),
                ProcessName = i.First().ProcessName,
                CurrentCpuUsage = i.Sum(c => c.CurrentCpuUsage),
            }).OrderByDescending(i => i.CurrentCpuUsage).Take(3));

        }

        #endregion Processes taking Max Cpu Usage
    }
}
