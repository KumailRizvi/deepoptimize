﻿using NLog;
using Phoenix.Windows.Common;
using System;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Phoenix.Windows.ViewModels
{
    class MainViewModel : BaseViewModel
    {
        #region Constants & Declarations
        const ProcessPriorityClass PRIORITY_HIGH = ProcessPriorityClass.High;
        private static Logger log = LogManager.GetCurrentClassLogger();
        #endregion Constants & Declarations

        #region Process CSV File
        /// <summary>
        /// Extracts processes name from CSV file and populate list with the names
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        internal List<string> ProcessContent(string filePath)
        {
            var ProcessesList = new List<string>();
            var delimiters = new char[] { ',' };
            using (var reader = new StreamReader(filePath))
            {
                while (!reader.EndOfStream)
                {
                    var processes = reader.ReadLine().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

                    ProcessesList.AddRange(processes);
                }
            }
            return ProcessesList;
        }

        #endregion Process CSV Files

        #region Priority Setting and Updation 

        /// <summary>
        /// Iterate processes list and pass the names to update their priority.
        /// </summary>
        /// <param name="ProcessesList"></param>
        /// <returns></returns>
        internal int UpdateProcessPriority(List<string> ProcessesList)
        {
            int prioritiesUpdated = 0;

            foreach (var eachProcess in ProcessesList)
            {
                try
                {
                    var result = SetProcessPriority(GetProcessByName(eachProcess), PRIORITY_HIGH);
                    if (result)
                    {
                        prioritiesUpdated++;
                    }
                }
                catch (Exception ex)
                {
                    log.Error($"Unable to set Priority for Process: {eachProcess}, {ex.StackTrace.ToString()}");
                }
            }

            return prioritiesUpdated;
        }

        /// <summary>
        /// Update processes priority
        /// </summary>
        /// <param name="Processes"></param>
        /// <param name="ProcessPriorityClass"></param>
        private bool SetProcessPriority(Process[] Processes, ProcessPriorityClass ProcessPriorityClass)
        {
            var PriorityUpdated = false;
            if (Processes != null)
            {
                foreach (var process in Processes)
                {
                    try
                    {
                        process.PriorityClass = ProcessPriorityClass;
                        PriorityUpdated = true;
                    }
                    catch (Exception ex)
                    {
                        log.Error($"Unable to set Priority for Process: {process},id: {process.Id}, {ex.StackTrace.ToString()}");
                        throw ex;
                    }
                }
            }
            return PriorityUpdated;
        }

        /// <summary>
        /// Get all instances of given process name running on the local computer.
        /// This will return an empty array if process isn't running.
        /// </summary>
        /// <param name="ProcessName"></param>
        /// <returns></returns>
        private Process[] GetProcessByName(string ProcessName) => Process.GetProcessesByName(ProcessName);

        #endregion Priority Setting and Updation 

        #region GetAllProcess
        /// <summary>
        /// Method not used for now. Implemented for future use.
        /// </summary>
        /// <returns></returns>
        internal List<ProcessItem> GetAllProcesses()
        {
            var ProcessesList = new List<ProcessItem>();

            var prss = Process.GetProcesses();
            foreach (var pr in Process.GetProcesses())
            {
                var proc = new ProcessItem();

                proc.Pid = pr.Id;
                proc.ProcessName = pr.ProcessName;
                proc.ApplicationName = GetProcessName(pr.Id);

                try
                {
                    proc.Priority = pr.PriorityClass;
                }
                catch (Exception ex)
                {
                    log.Error($"Unable to get Priority for Process: {proc.ProcessName} with pid: {proc.Pid}, {ex.StackTrace.ToString()}");
                    continue;
                }
                ProcessesList.Add(proc);
            }
            return ProcessesList;
        }


        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(uint processAccess, bool bInheritHandle, int processId);

        [DllImport("psapi.dll")]
        static extern uint GetModuleFileNameEx(IntPtr hProcess, IntPtr hModule, [Out] StringBuilder lpBaseName, [In] [MarshalAs(UnmanagedType.U4)] int nSize);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool CloseHandle(IntPtr hObject);


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string GetProcessName(int pid)
        {

            var processHandle = OpenProcess(0x0400 | 0x0010, false, pid);

            if (processHandle == IntPtr.Zero)
            {
                return null;
            }

            var sb = new StringBuilder(Constants.PROCESS_NAME_LENGTH);

            string result = null;

            if (GetModuleFileNameEx(processHandle, IntPtr.Zero, sb, Constants.PROCESS_NAME_LENGTH) > 0)
            {
                result = Path.GetFileName(sb.ToString());
            }

            CloseHandle(processHandle);

            return result;
        }

        #endregion GetAllProcess

    }
}
