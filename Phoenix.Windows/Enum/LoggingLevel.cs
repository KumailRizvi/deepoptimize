﻿namespace DeepArmor.Client.CommonLibrary.Enums
{
    public enum LoggingLevel
    {
        DEBUG,
        INFO,
        WARN,
        ERROR
    }
}