﻿namespace Phoenix.Windows.Enum
{
    internal enum WindowCurrentState : int
    {
        Hide = 0,
        Normal = 1,
        Minimized = 2,
        Maximized = 3,
    }
}
