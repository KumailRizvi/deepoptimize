﻿using Phoenix.Windows.Common;
using System.Windows;

namespace Phoenix.Windows
{
    /// <summary>
    /// Interaction logic for MessagePopup.xaml
    /// </summary>
    public partial class MessagePopup : Window
    {
        public MessagePopup(string message)
        {
            InitializeComponent();
            ShowInTaskbar = false;
            SetNotificationMessage(message);
        }

        private void BtnOk_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SetNotificationMessage(string message)
        {
            tbNotification.Text = message;
            Title = Constants.APP_NAME;
        }
    }
}
