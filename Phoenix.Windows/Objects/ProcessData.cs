﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Media.Imaging;
using Phoenix.Windows.Enum;
using Phoenix.Windows.ViewModels;

namespace Phoenix.Windows.Objects
{
    public class ProcessData  : BaseViewModel,INotifyPropertyChanged
    {
        private int _Pid;
        public int Pid
        {
            get { return _Pid; }
            set
            {
                _Pid = value;
                OnPropertyChanged("Pid");
            }
        }

        private string _ProcessName;
        public string ProcessName
        {
            get { return _ProcessName; }
            set
            {
                _ProcessName = value;
                OnPropertyChanged("ProcessName");
            }
        }

        private string _DisplayName;
        public string DisplayName
        {
            get { return _DisplayName; }
            set
            {
                _DisplayName = value;
                OnPropertyChanged("DisplayName");
            }
        }

        private string _ApplicationName;
        public string ApplicationName
        {
            get { return _ApplicationName; }
            set
            {
                _ApplicationName = value;
                OnPropertyChanged("ApplicationName");
            }
        }

        private string _action;
        public string action
        {
            get { return _action; }
            set
            {
                _action = value;
                OnPropertyChanged("action");
                TotalActions = actionsList.Count;
            }
        }

        private IList<string> _actionsList = new ObservableCollection<string>();
        public IList<string> actionsList
        {
            get { return _actionsList; }
            set
            {
                _actionsList = value;
                OnPropertyChanged("actionsList");
            }
        }

        private bool _IsReverted = false;
        public bool IsReverted
        {
            get { return _IsReverted; }
            set
            {
                _IsReverted = value;
                OnPropertyChanged("IsReverted");
            }
        }

        private ProcessPriorityClass _OrignalPriority;
        public ProcessPriorityClass OrignalPriority
        {
            get { return _OrignalPriority; }
            set
            {
                _OrignalPriority = value;
                OnPropertyChanged("OrignalPriority");
            }
        }

        private BitmapImage _ApplicationIcon;
        public BitmapImage ApplicationIcon
        {
            get { return _ApplicationIcon; }
            set
            {
                _ApplicationIcon = value;
                OnPropertyChanged("ApplicationIcon");
            }
        }

        private ProcessPriorityClass? _UpdatedPriority;
        public ProcessPriorityClass? UpdatedPriority
        {
            get { return _UpdatedPriority; }
            set
            {
                _UpdatedPriority = value;
                OnPropertyChanged("UpdatedPriority");
            }
        }

        private WindowCurrentState _CurrentWindowState;
        internal WindowCurrentState CurrentWindowState
        {
            get { return _CurrentWindowState; }
            set
            {
                _CurrentWindowState = value;
                OnPropertyChanged("CurrentWindowState");
            }
        }

        public bool IsAlive { get; set; }

        private int _TotalActions;
        public int TotalActions
        {
            get { return _TotalActions; }
            set
            {
                _TotalActions = value;
                OnPropertyChanged("TotalActions");
            }
        }

        private bool _IsTop3CpuUser = false;
        public bool IsTop3CpuUser
        {
            get { return _IsTop3CpuUser; }
            set
            {
                _IsTop3CpuUser = value;
                OnPropertyChanged("IsTop3CpuUser");
            }
        }

        private double _CurrentCpuUsage = 0.0f;
        public double CurrentCpuUsage
        {
            get { return _CurrentCpuUsage; }
            set
            {
                _CurrentCpuUsage = value;
                OnPropertyChanged("CurrentCpuUsage");
            }
        }

        private bool _IsFromCSV = false;
        public bool IsFromCSV
        {
            get { return _IsFromCSV; }
            set
            {
                _IsFromCSV = value;
                OnPropertyChanged("IsFromCSV");
            }
        }

        public void UpdateActions(string updateAction)
        {
            this.action = updateAction;
            this.actionsList.Insert(0, updateAction);
            this.actionsList = new ObservableCollection<string>(actionsList.Take(SystemTrayViewModel.PhoenixConfig.HistoryDisplayCount));
        }
    }
}
