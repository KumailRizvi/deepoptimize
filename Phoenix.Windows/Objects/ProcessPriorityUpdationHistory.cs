﻿using System.Diagnostics;

namespace Phoenix.Windows.Objects
{
    class ProcessPriorityUpdationHistory
    {
        public ProcessPriorityUpdationHistory(int id, int PID, string ProcessName, ProcessPriorityClass PreviousPriority, ProcessPriorityClass UpdatedPriority)
        {
            this.id = id;
            this.PID = PID;
            this.ProcessName = ProcessName;
            this.PreviousPriority = PreviousPriority;
            this.UpdatedPriority = UpdatedPriority;
        }
        public int id { get; set; }
        public int PID { get; set; }
        public string ProcessName { get; set; }
        public ProcessPriorityClass PreviousPriority { get; set; }

        public ProcessPriorityClass UpdatedPriority { get; set; }
    }
}
