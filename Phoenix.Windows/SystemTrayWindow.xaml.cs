﻿using Phoenix.Windows.Common;
using Phoenix.Windows.Controls;
using Phoenix.Windows.Objects;
using Phoenix.Windows.ViewModels;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Phoenix.Windows
{
    /// <summary>
    /// Interaction logic for SystemTrayWindow.xaml
    /// </summary>
    public partial class SystemTrayWindow : Window
    {
        private ICommand _taskbarIconDoubleClickCommand;
        private SystemTrayViewModel viewModel;
        private Timer aTimer;


        public SystemTrayWindow()
        {
            InitializeComponent();
            viewModel = new SystemTrayViewModel();
            if (DataContext == null || !(DataContext is SystemTrayViewModel))
            {
                DataContext = new SystemTrayViewModel();
            }
            aTimer = new Timer();
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            aTimer.Interval = 5000;
            aTimer.Enabled = true;

            Loaded += SystemTrayWindow_Loaded;
            this.MouseDown += Window_MouseDown;

            //Application.Current.Deactivated += MinimizeToTray;
            this.DisabledUI.Visibility = Visibility.Collapsed;
            tbiMain.LeftClickCommand = TaskbarIconDoubleClickCommand;
            tbiMain.DoubleClickCommand = TaskbarIconDoubleClickCommand;
        }

        /// <summary>
        /// To make window draggable
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        /// <summary>
        /// Initialize RelayCommand to invoke application from system tray on double click.
        /// </summary>
        private ICommand TaskbarIconDoubleClickCommand => _taskbarIconDoubleClickCommand ?? (_taskbarIconDoubleClickCommand = new RelayCommand(TaskbarIconDoubleClick));

        /// <summary>
        /// Bring application in front, show in taskbar and set window state to normal on icon double click from system tray.
        /// </summary>
        private void TaskbarIconDoubleClick()
        {
            Activate();
            ShowInTaskbar = true;
            WindowState = WindowState.Normal;
            Visibility = Visibility.Visible;

        }
        private BitmapImage LoadImage(string filename)
        {
            return new BitmapImage(new Uri($"{AppDomain.CurrentDomain.BaseDirectory}Images/{filename}"));
        }

        private void MinimizeToTray(object sender, EventArgs e)
        {
            Visibility = Visibility.Hidden;
        }

        private void SystemTrayWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var desktopWorkingArea = SystemParameters.WorkArea;

            Left = desktopWorkingArea.Right - Width - 5;
            Top = desktopWorkingArea.Bottom - Height;

            OnTimedEvent(null, null);
        }

        private void Open_Preview(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Normal;
            Visibility = Visibility.Visible;
            Activate();
        }

        /// <summary>
        /// Shutdown application when 'Exit' is clicked from system tray.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void ToggleSwitchButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var applicationstate = (sender as ToggleSwitchButton).IsChecked;

            if (applicationstate ?? false)
            {
                new System.Threading.Thread(() =>
                {

                    OnTimedEvent(null, null);
                }).Start();
                tbiMain.Icon = Properties.Resources.phoenix_icon;

                Uri enableIco = new Uri("pack://application:,,,/Phoenix;component/Images/phoenix-icon.ico", UriKind.RelativeOrAbsolute);
                this.Icon = BitmapFrame.Create(enableIco);
                this.DisabledUI.Visibility = Visibility.Collapsed;
                ProcessLV.Opacity = 1;

                aTimer.Start();
                viewModel.ListOfActiveProcesses = new ObservableCollection<ProcessData>();

                return;
            }

            Dispatcher.Invoke((Action)delegate ()
            {
                tbiMain.Icon = Properties.Resources.phoenix_icon_disable;

                Uri disableIco = new Uri("pack://application:,,,/Phoenix;component/Images/phoenix-icon-disable.ico", UriKind.RelativeOrAbsolute);
                this.Icon = BitmapFrame.Create(disableIco);
            });

            ProcessLV.Opacity = 0.5;
            aTimer.Stop();
            this.DisabledUI.Visibility = Visibility.Visible;
            this.ProcessLV.SelectedIndex = -1;

            Dispatcher.Invoke((Action)delegate ()
            {
                viewModel.RevertProcessToOrignalState(0, true);
                this.txtNumberOfAutoUpdatedPriorities.Text = ("0").ToString();
            });

        }

        #region Timer Function
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            Dispatcher.Invoke((Action)delegate ()
            {
                var CSVProcessesList = viewModel.ProcessContent(@".\Phoenix-sample-csv.csv");

                viewModel.UpdateProcessPriorityFromCSV(CSVProcessesList);

                 viewModel.GetAllProcesses();

                viewModel.UpdateProcessPriority(viewModel.ListOfActiveProcesses, CSVProcessesList);
                var DisplayItems = viewModel.ListOfActiveProcesses.Where(dp => dp.UpdatedPriority != System.Diagnostics.ProcessPriorityClass.Normal
                                                                               && dp.UpdatedPriority != System.Diagnostics.ProcessPriorityClass.AboveNormal);

                this.ProcessLV.ItemsSource = viewModel.ListOfDisplayProcesses = new ObservableCollection<ProcessData>(DisplayItems);
                this.txtNumberOfAutoUpdatedPriorities.Text = viewModel.ListOfDisplayProcesses.Count.ToString();
                this.ProcessLV.SelectedIndex = -1;

                GC.Collect();
            });
        }
        #endregion


        private void RevertButton_Click(object sender, RoutedEventArgs e)
        {
            Button revert = (Button)(sender);
            viewModel.RevertProcessToOrignalState(Convert.ToInt32(revert.Tag));
        }

        /// <summary>
        /// Minimize application to taskbar when clicked on '_' image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnMinimize_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        /// <summary>
        /// Hide application from front when clicked on 'x' image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
            ShowInTaskbar = false;
        }
    }
}
