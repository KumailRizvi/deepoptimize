﻿using System;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Specialized;


namespace Phoenix.Windows.ExtensionMethods
{

    public class TrulyObservableCollection<T> : ObservableCollection<T>
    {

        public TrulyObservableCollection()
            : base()
        {
            CollectionChanged += new NotifyCollectionChangedEventHandler(TrulyObservableCollection_CollectionChanged);
        }

        void TrulyObservableCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Object item in e.NewItems)
                {
                    (item as INotifyPropertyChanged).PropertyChanged += new PropertyChangedEventHandler(item_PropertyChanged);
                }
            }
            if (e.OldItems != null)
            {
                foreach (Object item in e.OldItems)
                {
                    (item as INotifyPropertyChanged).PropertyChanged -= new PropertyChangedEventHandler(item_PropertyChanged);
                }
            }

            item_PropertyChanged(null, null);
        }


        public delegate void MyEventHandler();

        public event MyEventHandler ItemChanged;

        void item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (ItemChanged != null)
            {
                ItemChanged();
            }
        }
    }
}
