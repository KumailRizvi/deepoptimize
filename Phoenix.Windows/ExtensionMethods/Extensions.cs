﻿
using System.Linq;

namespace Phoenix.Windows.ExtensionMethods
{
    public static class Extensions
    {
       public static string ToPhoenixPriorityString(this string str)
        {
            switch (str)
            {
                case "RealTime":
                    return "Optimized Execution";

                case "High":
                    return "Optimized Execution";

                case "AboveNormal":
                    return "Normal Execution";

                case "Normal":
                    return "Normal Execution";

                case "BelowNormal":
                    return "Below Normal Execution";

                case "Low":
                    return "Below Normal Execution";

                default:
                    return "Normal Execution";
            }
        }

        public static string ToFirstLetterUpper(this string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                return input.First().ToString().ToUpper() + input.Substring(1);
            }

            return string.Empty;
        }
    }
}
